﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace PCloud2Surface
{
    public class PCloud2SurfaceInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "PCloud2Surface";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                //Return a 24x24 pixel bitmap to represent this GHA library.
                return null;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("1a80a9f3-f1e6-4d4c-b1c1-6a9aef980437");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "";
            }
        }
    }
}
