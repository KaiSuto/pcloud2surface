﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using PCloud2Surface.Classes;

namespace PCloud2Surface.Components
{
    public class GetPoints : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the GetPoints class.
        /// </summary>
        public GetPoints()
          : base("Get Points", "Get Points",
              "Get Points from RBF",
              "Interpolation", "Point")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("RBF", "RBF", "A RBF Planar interpolation instance.", GH_ParamAccess.item);
            pManager.AddNumberParameter("X", "X", "A x coordinate of a planar coordinates.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Y", "Y", "A y coordinate of a planar coordinates.", GH_ParamAccess.list);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddPointParameter("Points", "Pts", "Points", GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            RBFInterpolation rbf = new RBFInterpolation();
            List<double> x = new List<double>();
            List<double> y = new List<double>();

            DA.GetData(0, ref rbf);
            DA.GetDataList(1, x);
            DA.GetDataList(2, y);

            List<Point3d> pts = new List<Point3d>();
            for (int i = 0; i < x.Count; i++)
            {
                Point3d pt = rbf.P(x[i], y[i]);
                pts.Add(pt);
            }

            DA.SetDataList(0, pts);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //You can add image files to your project resources and access them like this:
                // return Resources.IconForThisComponent;
                return Properties.Resource.IconPoints;
            }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("1a0e6594-24ae-4b1e-9682-75826e86f499"); }
        }
    }
}