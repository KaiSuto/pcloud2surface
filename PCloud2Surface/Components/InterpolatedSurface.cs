﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using PCloud2Surface.Classes;
using Rhino.Geometry;

namespace PCloud2Surface.Components
{
    public class InterpolatedSurface : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the InterpolatedSurface class.
        /// </summary>
        public InterpolatedSurface()
          : base("Interpolated Surface", "IS",
              "Create the interpolated surface form RBF",
              "Interpolation", "Surface")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("RBF", "RBF", "A RBF Planar interpolation instance.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Start X", "SX", "A start x coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddNumberParameter("End X", "EX", "A end x coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddNumberParameter("Start Y", "SY", "A start y coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddNumberParameter("End Y", "EY", "A end y coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddIntegerParameter("U Count", "UC", "A count of control points at x coordinate", GH_ParamAccess.item);
            pManager.AddIntegerParameter("V Count", "VC", "A count of control points at y coordinate", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddSurfaceParameter("Interpolated Surface", "IS", "A interpolated surface from RBF.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            RBFInterpolation rbf = new RBFInterpolation();
            double xStart = 0;
            double xEnd = 10;
            double yStart = 0;
            double yEnd = 10;
            int uCount = 10;
            int vCount = 10;

            DA.GetData(0, ref rbf);
            DA.GetData(1, ref xStart);
            DA.GetData(2, ref xEnd);
            DA.GetData(3, ref yStart);
            DA.GetData(4, ref yEnd);
            DA.GetData(5, ref uCount);
            DA.GetData(6, ref vCount);

            Surface interpolatedSurface = rbf.InterpolatedSurface(xStart, xEnd, yStart, yEnd, uCount, vCount);

            DA.SetData(0, interpolatedSurface);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //You can add image files to your project resources and access them like this:
                // return Resources.IconForThisComponent;
                return Properties.Resource.IconSurface;
            }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("fd5133a7-80e8-4d31-865c-2fefec5e5104"); }
        }
    }
}